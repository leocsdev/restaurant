# Restaurant Practice App

Main project for teaching basics to with node server.
This project has multiple branches with rising level of difficulty. Each branch is building on the concepts of before

### Installation
```
npm install
```

### Running 
```
npm run start
```